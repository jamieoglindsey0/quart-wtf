#!/usr/bin/env python3.8
from setuptools import find_packages, setup

with open('README.rst') as f:
    readme = f.read()

setup(
    name='Quart-WTF',
    version='0.0.1-alpha_1',
    url='https://gitlab.com/jamieoglindsey0/quart-wtf',
    license='BSD',
    author='Jamie Lindsey',
    author_email='your.python.solutions@gmail.com',
    maintainer='Jamie Lindsey',
    maintainer_email='your.python.solutions@gmail.com',
    description='Simple integration of Quart and WTForms.',
    long_description=readme,
    packages=find_packages(exclude=('tests',)),
    zip_safe=False,
    platforms='any',
    install_requires=[
        'Quart',
        'WTForms',
        'itsdangerous', 'requests'
    ],
    classifiers=[
        'Development Status :: 1 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Flask',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
