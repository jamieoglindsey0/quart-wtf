# -*- coding: utf-8 -*-

from quart import Markup, current_app
from werkzeug.urls import url_encode


RECAPTCHA_SCRIPT = "https://www.google.com/recaptcha/api.js"

__all__ = ["RecaptchaWidget"]


def recaptcha_html(public_key: str):
    html = current_app.config.get("RECAPTCHA_HTML")
    if html:
        return Markup(html)
    params = current_app.config.get("RECAPTCHA_PARAMETERS")
    script = RECAPTCHA_SCRIPT
    if params:
        script += f"?{url_encode(params)}"

    attrs = current_app.config.get("RECAPTCHA_DATA_ATTRS", {})
    attrs["sitekey"] = public_key
    snippet = " ".join([f'data-{i}="{attrs[i]}"' for i in attrs])
    return Markup(
        f"""<script src='{script}' async defer></script>\
            <div class="g-recaptcha" {snippet}></div>"""
    )


class RecaptchaWidget:

    def __call__(self, field, error=None, **kwargs):
        """Returns the recaptcha input HTML."""

        try:
            public_key = current_app.config["RECAPTCHA_PUBLIC_KEY"]
        except KeyError:
            raise RuntimeError("RECAPTCHA_PUBLIC_KEY config not set")

        return recaptcha_html(public_key)
