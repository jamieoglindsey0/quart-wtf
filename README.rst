Quart-WTF
=========

Simple integration of Quart and WTForms, including CSRF, file upload, and reCAPTCHA.

Links
-----

* `Documentation <https://quart-wtf.readthedocs.io>`_
* `PyPI <https://pypi.python.org/pypi/Quart-WTF>`_
* `GitLab <https://gitlab.com/jamieoglindsey0/quart-wtf>`_
